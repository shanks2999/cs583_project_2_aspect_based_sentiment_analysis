# CS583_Project_2_Aspect_Based_Sentiment_Analysis

Given an aspect term (also called opinion target) in a sentence, predict the sentiment label for the aspect term in the sentence.